# Log

## What is it
Class for console logging with multiple levels

## Installation
```
yarn add @lightsourcee/log
```
OR
```
npm install @lightsourcee/log
```

## Example of usage

```
import Log from '@lightsourcee/log';

Log.setAppName('Rabbit');
Log.setIsDebugMode(true);
Log.setErrorCallback((level, message, debugArgs) => {
    // TODO
});

Log.write(Log.level.DEBUG, 'My color is a', {name: 'white'});
```
## Example of output
```
Rabbit : DEBUG : My color is a : 17.3
Object { name: "white" }
```